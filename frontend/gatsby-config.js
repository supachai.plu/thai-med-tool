module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#000000`,
        theme_color: `#000000`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-less`,
      options: {
        javascriptEnabled: true,
        cssLoaderOptions: {
          camelCase: false,
        },
        modifyVars: {
          'border-radius-base': `4px`,
          'primary-color': `#C14C40`,
          'link-color': `#C14C40`,
          'menu-dark-item-active-bg': `#46494C`,
          'menu-dark-submenu-bg': `#FFFFFF`,
          'menu-dark-bg' : '#6A3934',
          'menu-bg': '#C14C40',
          'menu-highlight-color': `#FFFFFF`,
          'layout-sider-background': `#6A3934`,
          'layout-header-background': `#C14C40`,
          'layout-body-background': `#EDEDF0`,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-antd',
      options: {
        style: true
      }
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
