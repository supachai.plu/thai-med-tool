/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import React from 'react'
import { Provider } from 'react-redux'

import createStore from './src/state/store'
import LayoutProject from './src/pages/layout'
import 'antd/dist/antd.less'

export const wrapRootElement = ({ element }) => {

    return (
        <Provider store={createStore()}>
            <LayoutProject>
                {element}
            </LayoutProject>
        </Provider>
    )
    
}
