import path from 'path';

exports.onCreateWebpackConfig = ({ actions, stage, loaders }) => {
  // If production JavaScript and CSS build
  
  if (stage === 'build-javascript') {
    // Turn off source maps
    actions.setWebpackConfig({
      devtool: false,
    })
  }
  
 if (stage === "build-html") {
  actions.setWebpackConfig({
    module: {
      rules: [
        {
          test: /bad-module/,
          use: loaders.null(),
        },
      ],
    },
  })
}
};