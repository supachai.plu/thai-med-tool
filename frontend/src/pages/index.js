import React, { useEffect, useState } from "react"
import { Link } from "gatsby"

import { v4 as uuidv4 } from 'uuid';

import io from 'socket.io-client';
const socket = io('http://localhost:3000');

var Widget = undefined;
var addResponseMessage = () => {};
var addUserMessage = () => {};
var addLinkSnippet = () => {};

if( typeof window !== 'undefined' ){
    Widget = require('react-chat-widget').Widget;
    addResponseMessage = require('react-chat-widget').addResponseMessage;
    addUserMessage =  require('react-chat-widget').addUserMessage;
    addLinkSnippet = require('react-chat-widget').addLinkSnippet;
}

const IndexPage = () => {

    const [id, setId] = useState(uuidv4());

    useEffect(() => {
        addResponseMessage('Welcome to this awesome chat!');

        //socket.on('connect', function(){});
        //socket.on('event', function(data){});
        //socket.on('disconnect', function(){});

        socket.emit( '_join', { id: id } );

        socket.on('response message', (data) => {
            console.log( `response message`, data )
            addResponseMessage(data);
        });

        //socket.emit('chat message', new Date() +` -> Hello` );

    }, [])

    const handleNewUserMessage = (newMessage) => {
        console.log(`New message incoming! ${newMessage}`);
        // Now send the message throught the backend API
        addResponseMessage(newMessage);
    };
    
    const handleSubmit = (values) => {
        console.log( values )
        addUserMessage(values)
        //addLinkSnippet({ link: 'https://google.co.th', title: 'test', target: '_blank' })
        document.getElementsByClassName('rcw-new-message')[0].value = '';
        socket.emit('chat message', { id: id, msg: `${values}` });
    }


    return (
        <>
            {
                typeof window !== 'undefined' && Widget && 
                <Widget 
                    handleNewUserMessage={handleNewUserMessage}
                    handleSubmit={handleSubmit}
                    title="Thai Med Tool Chat"
                    subtitle="Type your Question"
                /> 
            }
            <div style={{ height: '700px' }}></div>

            <section className="about-area pt-70">
                <div className="container">
                    <div className="row" style={{ display: 'flex' }}>
                        <div className="col-lg-6" style={{ flex: '1 1 50%' }}>
                            <div className="about-content mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.5s">
                                <div className="section-title">
                                    <div className="line"></div>
                                    <h3 className="title"><span>Crafted for</span> SaaS, App and Software Landing Page</h3>
                                </div>
                                <p className="text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing.</p>
                                <a href="#" className="main-btn">Try it Free</a>
                            </div> 
                        </div>
                        <div className="col-lg-6" style={{ flex: '1 1 50%' }}>
                            <div className="about-image text-center mt-50 wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="0.5s">
                                <img src="assets/images/about3.svg" alt="about" />
                            </div>
                        </div>
                    </div> 
                </div>
            </section>
            

            <section id="facts" className="video-counter pt-70" style={{ paddingBottom: '40px' }}>
                <div className="container">
                    <div className="row" style={{ display: 'flex' }}>
                        <div className="col-lg-6" style={{ flex: '1 1 50%' }}>
                            <div className="video-content mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                <img className="dots" src="assets/images/dots.svg" alt="dots" />
                                <div className="video-wrapper">
                                    <div className="video-image">
                                        <img src="assets/images/video.png" alt="video" />
                                    </div>
                                    <div className="video-icon">
                                        <a href="https://www.youtube.com/watch?v=r44RKWyfcFw" className="video-popup"><i className="lni-play"></i></a>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div className="col-lg-6" style={{ flex: '1 1 50%' }}>
                            <div className="counter-wrapper mt-50 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                <div className="counter-content">
                                    <div className="section-title">
                                        <div className="line"></div>
                                        <h3 className="title">Cool facts <span> this about app</span></h3>
                                    </div>
                                    <p className="text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, seiam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                                </div> 
                                <div className="row no-gutters" style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <div className="col-4" style={{ width: '100%'}}>
                                        <div className="single-counter counter-color-1 d-flex align-items-center justify-content-center" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                            <div className="counter-items text-center">
                                                <span className="count"><span className="counter">125</span>K</span>
                                                <p className="text">Downloads</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-4" style={{ width: '100%'}}>
                                        <div className="single-counter counter-color-2 d-flex align-items-center justify-content-center" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                            <div className="counter-items text-center">
                                                <span className="count"><span className="counter">87</span>K</span>
                                                <p className="text">Active Users</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-4" style={{ width: '100%'}}>
                                        <div className="single-counter counter-color-3 d-flex align-items-center justify-content-center" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                            <div className="counter-items text-center">
                                                <span className="count"><span className="counter">4.8</span></span>
                                                <p className="text">User Rating</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section>


            <section className="features-extended section">
                <div className="container">
                    <div className="features-extended-inner section-inner has-top-divider">

                        <div className="features-extended-header text-center wow fadeInDown" style={{ marginBottom: '56px' }}>
                            <div className="container-sm">
                                <h2 className="section-title mt-0">Our Service</h2>
                                <p className="section-paragraph">Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a document or visual presentation</p>
                            </div>
                        </div>

                        <div className="feature-extended wow fadeInLeft" style={{ display: 'flex', marginBottom: '40px' }}>
                             <div className="feature-extended-image " style={{ flex: '1 1 40%' }}>
                                <svg width="480" height="360" viewBox="0 0 480 360" xmlns="http://www.w3.org/2000/svg">
                                    <defs>
                                        <filter x="-500%" y="-500%" width="1000%" height="1000%" filterUnits="objectBoundingBox" id="dropshadow-1">
                                            <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter"/>
                                            <feGaussianBlur stdDeviation="24" in="shadowOffsetOuter" result="shadowBlurOuter"/>
                                            <feColorMatrix values="0 0 0 0 0.12 0 0 0 0 0.17 0 0 0 0 0.21 0 0 0 0.2 0" in="shadowBlurOuter"/>
                                        </filter>
                                    </defs>
                                    <path fill="#F6F8FA" d="M0 220V0h200zM480 140v220H280z"/>
                                    <path fill="#FFF" d="M40 50h400v260H40z"/>
                                    <path fill="#FFF" d="M103 176h80v160h-80zM320 24h88v88h-88z"/>
                                    <path fill="#02C6A4" d="M230.97 198l16.971 16.971-16.97 16.97L214 214.972z"/>
                                    <path fill="#84E482" d="M203 121H103v100z"/>
                                </svg>
                            </div>
                            <div className="feature-extended-body" style={{ flex: '1 1 40%' }}>
                                <h3 className="mt-0">Discover</h3>
                                <p>Where text is visible, people tend to focus on the textual content rather than upon overall presentation</p>
                            </div>
                        </div>
                        <div className="feature-extended wow fadeInRight" style={{ display: 'flex', marginBottom: '40px' }}>
                            <div className="feature-extended-body" style={{ flex: '1 1 40%' }}>
                                <h3 className="mt-0">Discover</h3>
                                <p>Where text is visible, people tend to focus on the textual content rather than upon overall presentation</p>
                            </div>
                            <div className="feature-extended-image " style={{ flex: '1 1 40%' }}>
                                <svg width="480" height="360" viewBox="0 0 480 360" xmlns="http://www.w3.org/2000/svg">
                                    <defs>
                                        <filter x="-500%" y="-500%" width="1000%" height="1000%" filterUnits="objectBoundingBox" id="dropshadow-2">
                                            <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter"/>
                                            <feGaussianBlur stdDeviation="24" in="shadowOffsetOuter" result="shadowBlurOuter"/>
                                            <feColorMatrix values="0 0 0 0 0.12 0 0 0 0 0.17 0 0 0 0 0.21 0 0 0 0.2 0" in="shadowBlurOuter"/>
                                        </filter>
                                    </defs>
                                    <path fill="#F6F8FA" d="M0 220V0h200zM480 140v220H280z"/>
                                    <path fill="#FFF" d="M40 50h400v260H40z"/>
                                    <path fill="#FFF" d="M103 176h80v160h-80zM320 24h88v88h-88z"/>
                                    <path fill="#02C6A4" d="M230.97 198l16.971 16.971-16.97 16.97L214 214.972z"/>
                                    <path fill="#84E482" d="M203 121H103v100z"/>
                                </svg>
                            </div>

                        </div>
                        <div className="feature-extended wow fadeInLeft" style={{ display: 'flex', marginBottom: '40px' }}>
                            <div className="feature-extended-image" style={{ flex: '1 1 40%' }} >
                                <svg width="480" height="360" viewBox="0 0 480 360" xmlns="http://www.w3.org/2000/svg">
                                    <defs>
                                        <filter x="-500%" y="-500%" width="1000%" height="1000%" filterUnits="objectBoundingBox" id="dropshadow-3">
                                            <feOffset dy="16" in="SourceAlpha" result="shadowOffsetOuter"/>
                                            <feGaussianBlur stdDeviation="24" in="shadowOffsetOuter" result="shadowBlurOuter"/>
                                            <feColorMatrix values="0 0 0 0 0.12 0 0 0 0 0.17 0 0 0 0 0.21 0 0 0 0.2 0" in="shadowBlurOuter"/>
                                        </filter>
                                    </defs>
                                    <path fill="#F6F8FA" d="M0 220V0h200zM480 140v220H280z"/>
                                    <path fill="#FFF" d="M40 50h400v260H40z"/>
                                    <path fill="#FFF" d="M103 176h80v160h-80zM320 24h88v88h-88z"/>
                                    <path fill="#02C6A4" d="M230.97 198l16.971 16.971-16.97 16.97L214 214.972z"/>
                                    <path fill="#84E482" d="M203 121H103v100z"/>
                                </svg>
                            </div>
                            <div className="feature-extended-body" style={{ flex: '1 1 40%' }}>
                                <h3 className="mt-0">Discover</h3>
                                <p>Where text is visible, people tend to focus on the textual content rather than upon overall presentation</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            

            <section id="feature" className="features section text-center">

                <div className="row">
                    <h3 className="title"><span>Our Recent</span> Feature</h3>
                </div>

                <div className="section-square"></div>
                <div className="container">
                    <div className="features-inner section-inner">
                        <div className="features-wrap">

                            <div className="feature  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.2s">
                                <div className="feature-inner">
                                    <div className="feature-icon">
                                        <svg width="48" height="48" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                                <linearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="feature-1-a">
                                                    <stop stopColor="#007CFE" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#007DFF" offset="100%" />
                                                </linearGradient>
                                                <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="feature-1-b">
                                                    <stop stopColor="#FF4F7A" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#FF4F7A" offset="100%" />
                                                </linearGradient>
                                            </defs>
                                            <g fill="none" fillRule="evenodd">
                                                <path d="M8 0h24v24a8 8 0 0 1-8 8H0V8a8 8 0 0 1 8-8z" fill="url(#feature-1-a)" />
                                                <path d="M48 16v24a8 8 0 0 1-8 8H16c0-17.673 14.327-32 32-32z" fill="url(#feature-1-b)" />
                                            </g>
                                        </svg>
                                    </div>
                                    <h4 className="feature-title h3-mobile">Feature</h4>
                                    <p className="text-sm">A pseudo-Latin text used in web design, layout, and printing in place of English to emphasise design elements.</p>
                                </div>
                            </div>

                            <div className="feature  wow fadeInDown" data-wow-duration="1.5s" data-wow-delay="0.2s">
                                <div className="feature-inner">
                                    <div className="feature-icon">
                                        <svg width="48" height="48" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                                <linearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="feature-2-a">
                                                    <stop stopColor="#007CFE" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#007DFF" offset="100%" />
                                                </linearGradient>
                                                <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="feature-2-b">
                                                    <stop stopColor="#FF4F7A" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#FF4F7A" offset="100%" />
                                                </linearGradient>
                                            </defs>
                                            <g fill="none" fillRule="evenodd">
                                                <path d="M0 0h32v7c0 13.807-11.193 25-25 25H0V0z" fill="url(#feature-2-a)" />
                                                <path d="M48 16v7c0 13.807-11.193 25-25 25h-7c0-17.673 14.327-32 32-32z" fill="url(#feature-2-b)" transform="matrix(1 0 0 -1 0 64)" />
                                            </g>
                                        </svg>

                                    </div>
                                    <h4 className="feature-title h3-mobile">Feature</h4>
                                    <p className="text-sm">A pseudo-Latin text used in web design, layout, and printing in place of English to emphasise design elements.</p>
                                </div>
                            </div>

                            <div className="feature  wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.2s">
                                <div className="feature-inner">
                                    <div className="feature-icon">
                                        <svg width="48" height="48" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                                <linearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="feature-3-a">
                                                    <stop stopColor="#007CFE" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#007DFF" offset="100%" />
                                                </linearGradient>
                                                <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="feature-3-b">
                                                    <stop stopColor="#FF4F7A" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#FF4F7A" offset="100%" />
                                                </linearGradient>
                                            </defs>
                                            <g fill="none" fillRule="evenodd">
                                                <circle fill="url(#feature-3-a)" cx="16" cy="16" r="16" />
                                                <path d="M16 16c17.673 0 32 14.327 32 32H16V16z" fill="url(#feature-3-b)" />
                                            </g>
                                        </svg>

                                    </div>
                                    <h4 className="feature-title h3-mobile">Feature</h4>
                                    <p className="text-sm">A pseudo-Latin text used in web design, layout, and printing in place of English to emphasise design elements.</p>
                                </div>
                            </div>

                            <div className="feature  wow fadeInDown" data-wow-duration="2.5s" data-wow-delay="0.2s">
                                <div className="feature-inner">
                                    <div className="feature-icon">
                                        <svg width="48" height="48" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                                <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="feature-4-a">
                                                    <stop stopColor="#FF4F7A" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#FF4F7A" offset="100%" />
                                                </linearGradient>
                                                <linearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="feature-4-b">
                                                    <stop stopColor="#007CFE" stopOpacity="0" offset="0%" />
                                                    <stop stopColor="#007DFF" offset="100%" />
                                                </linearGradient>
                                            </defs>
                                            <g fill="none" fillRule="evenodd">
                                                <path d="M32 16h16v16c0 8.837-7.163 16-16 16H16V32c0-8.837 7.163-16 16-16z" fill="url(#feature-4-a)" />
                                                <path d="M16 0h16v16c0 8.837-7.163 16-16 16H0V16C0 7.163 7.163 0 16 0z" fill="url(#feature-4-b)" />
                                            </g>
                                        </svg>

                                    </div>
                                    <h4 className="feature-title h3-mobile">Feature</h4>
                                    <p className="text-sm">A pseudo-Latin text used in web design, layout, and printing in place of English to emphasise design elements.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section id="blog" className="blog-area pt-60">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="section-title pb-35">
                                <div className="line"></div>
                                <h3 className="title"><span>Our Recent</span> Blog Posts</h3>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-7">
                            <div className="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                                <div className="blog-image">
                                    <img src="assets/images/blog-1.jpg" alt="blog" />
                                </div>
                                <div className="blog-content">
                                    <ul className="meta">
                                        <li>Posted By: <a href="#">Admin</a></li>
                                        <li>03 June, 2023</li>
                                    </ul>
                                    <p className="text">Lorem ipsuamet conset sadips cing elitr seddiam nonu eirmod tempor invidunt labore.</p>
                                    <a className="more" href="#">Learn More <i className="lni-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-7">
                            <div className="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                <div className="blog-image">
                                    <img src="assets/images/blog-2.jpg" alt="blog" />
                                </div>
                                <div className="blog-content">
                                    <ul className="meta">
                                        <li>Posted By: <a href="#">Admin</a></li>
                                        <li>03 June, 2023</li>
                                    </ul>
                                    <p className="text">Lorem ipsuamet conset sadips cing elitr seddiam nonu eirmod tempor invidunt labore.</p>
                                    <a className="more" href="#">Learn More <i className="lni-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-7">
                            <div className="single-blog mt-30 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                <div className="blog-image">
                                    <img src="assets/images/blog-3.jpg" alt="blog" />
                                </div>
                                <div className="blog-content">
                                    <ul className="meta">
                                        <li>Posted By: <a href="#">Admin</a></li>
                                        <li>03 June, 2023</li>
                                    </ul>
                                    <p className="text">Lorem ipsuamet conset sadips cing elitr seddiam nonu eirmod tempor invidunt labore.</p>
                                    <a className="more" href="#">Learn More <i className="lni-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="contact" className="contact-area pt-125 pb-130 gray-bg">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="section-title text-center pb-20">
                                <h5 className="sub-title mb-15">Contact us</h5>
                                <h2 className="title">Get In touch</h2>
                            </div>
                        </div>
                    </div> 
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="contact-form">
                                <form id="contact-form" action="assets/contact.php" method="post" data-toggle="validator">
                                    <div className="row" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                        <div className="col-md-6" style={{ width: '48%' }}>
                                            <div className="single-form form-group">
                                                <input type="text" name="name" placeholder="Your Name" data-error="Name is required." required="required" />
                                                <div className="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div className="col-md-6" style={{ width: '48%' }}>
                                            <div className="single-form form-group">
                                                <input type="email" name="email" placeholder="Your Email" data-error="Valid email is required." required="required" />
                                                <div className="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div className="col-md-6" style={{ width: '48%' }}>
                                            <div className="single-form form-group">
                                                <input type="text" name="subject" placeholder="Subject" data-error="Subject is required." required="required" />
                                                <div className="help-block with-errors"></div>
                                            </div> 
                                        </div>
                                        <div className="col-md-6" style={{ width: '48%' }}>
                                            <div className="single-form form-group">
                                                <input type="text" name="phone" placeholder="Phone" data-error="Phone is required." required="required" />
                                                <div className="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div className="col-md-12" style={{ width: '100%' }}>
                                            <div className="single-form form-group">
                                                <textarea placeholder="Your Mesaage" name="message" data-error="Please,leave us a message." required="required"></textarea>
                                                <div className="help-block with-errors"></div>
                                            </div> 
                                        </div>
                                        <p className="form-message"></p>
                                        <div className="col-md-12" style={{ width: '100%', textAlign: 'center' }}>
                                            <div className="single-form form-group text-center">
                                                <button type="submit" className="main-btn">send message</button>
                                            </div>
                                        </div>
                                    </div> 
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default IndexPage
