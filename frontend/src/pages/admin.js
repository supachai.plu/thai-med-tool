import React, { useEffect, useState } from "react"
import Layout from "../components/layout"
import axios from 'axios'
import io from 'socket.io-client';

import { v4 as uuidv4 } from 'uuid';

const socket = io('http://localhost:3000');

const AdminPage = () => {

    const [id, setId] = useState(uuidv4());
    const [clients, setClients] = useState({});
    
    useEffect( () => {  

        socket.emit( '_join_admin', { id: id } );

        socket.on('client_data', (clients) => {
            setClients(clients)
            console.log('client_data', clients ) 
        })
        /*
        const fetchData = async () => {
            console.log( 'http://localhost:3000/clients' )
            const result = await axios.get('http://localhost:3000/clients')
            console.log( result )
        }

        fetchData();
        */
    }, [])

    const checkEnter = (e, value) => {
        if( e.keyCode == '13' ){
            socket.emit( 'admin message', { clientId: e.target.id, msg: e.target.value } );
        }
        //console.log( e.keyCode, e.target.value )
    }

    return (
        <div style={{ marginTop: '80px', color: 'black' }}>
        {
            Object.keys(clients).map(function(key) {
                return (<>
                    <div>Key: { [key] }</div>
                    <div>{ clients[key].msg.toString() }</div>
                    <input type="text" id={key} onKeyDown={checkEnter} />
                    <br/>
                </>)
            })
        }
        </div>
    )
}

export default AdminPage
