import React, { Component, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled, {css} from 'styled-components';
import { Layout, Menu } from 'antd';
import { navigateTo, navigate } from 'gatsby';
import queryString from 'query-string'
import { Helmet } from 'react-helmet';
import { useGlobalEvent, useWindowScroll } from 'beautiful-react-hooks'; 
//import wow from 'wow.js'
//import { addResponseMessage } from 'react-chat-widget';
import 'react-chat-widget/lib/styles.css';

import './animate.css'
import './index.css'


var wow = undefined;

if( typeof window !== 'undefined' ){
    wow = require('wow.js');
}

const { Header, Content, Footer } = Layout;

const LayoutContainer = styled.div`
    min-height: 100vh;
    min-width: 100%;
    max-width: 100%;
`

const LayoutProject = ({ children }) => {

    const [scrollY, setScrollY] = useState(undefined);

    useWindowScroll((event) => {
        if( typeof window != 'undefined' ){
            setScrollY(window.scrollY);
        }

        if (scrollY < 20) {
            document.getElementById('navbar-area').classList.remove('sticky')
            //$(".navbar-area").removeClass("sticky");
            //$(".navbar-area img").attr("src", "assets/images/logo.svg");
        } else {
            document.getElementById('navbar-area').classList.add('sticky')
            //$(".navbar-area").addClass("sticky");
            //$(".navbar-area img").attr("src", "assets/images/logo-2.svg");
        }
        //console.log( scrollY )

    });

    useEffect( () => {

        new wow({ 
            resetAnimation: true,
            scrollContainer: null, 
        }).init();

    }, [])

    return(
    <>
        <LayoutContainer> 
            
            <Helmet title="Thai Med Tool">
                <meta charSet="utf-8" />
                <title>Thai Med Tool</title>
                <meta name="title" content={'Thai Med Tool'}  />
                <meta name="description" content={'Thai Med Tool'} />

                <meta name="og:title" content={'Thai Med Tool'}  />
                <meta name="og:description" content={'Thai Med Tool'} />
                
                <link rel="canonical" href="" />
            </Helmet>

            <div>
                <Menu id={'navbar-area'} mode="horizontal" defaultSelectedKeys={['1']} style={{textAlign: 'right'}}>
                    <Menu.Item key="1">nav 1</Menu.Item>
                    <Menu.Item key="2" onClick={ () => { navigateTo('/#feature') } }>Feature</Menu.Item>
                    <Menu.Item key="3" onClick={ () => { navigateTo('/#blog') } }>Blog</Menu.Item>
                    <Menu.Item key="4" onClick={ () => { navigateTo('/#contact') } }>Contact Us</Menu.Item>
                </Menu>
            </div>

            <Layout>
                { children }
            </Layout>

            <Footer>
                <div className="container">
                    <div className="site-footer-inner has-top-divider">
                        <div className="brand footer-brand">
                            <a href="#">
                                <svg width="48" height="32" viewBox="0 0 48 32" xmlns="http://www.w3.org/2000/svg">
                                    <title>Agnes</title>
                                    <defs>
                                        <linearGradient x1="0%" y1="100%" y2="0%" id="logo-footer-a">
                                            <stop stopColor="#007CFE" stopOpacity="0" offset="0%"/>
                                            <stop stopColor="#007DFF" offset="100%"/>
                                        </linearGradient>
                                        <linearGradient x1="100%" y1="50%" x2="0%" y2="50%" id="logo-footer-b">
                                            <stop stopColor="#FF4F7A" stopOpacity="0" offset="0%"/>
                                            <stop stopColor="#FF4F7A" offset="100%"/>
                                        </linearGradient>
                                    </defs>
                                    <g fill="none" fillRule="evenodd">
                                        <rect fill="url(#logo-footer-a)" width="32" height="32" rx="16"/>
                                        <rect fill="url(#logo-footer-b)" x="16" width="32" height="32" rx="16"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <ul className="footer-links list-reset">
                            <li>
                                <a href="#">Contact</a>
                            </li>
                            <li>
                                <a href="#">About us</a>
                            </li>
                            <li>
                                <a href="#">FAQ's</a>
                            </li>
                            <li>
                                <a href="#">Support</a>
                            </li>
                        </ul>
                        <ul className="footer-social-links list-reset">
                            <li>
                                <a href="#">
                                    <span className="screen-reader-text">Facebook</span>
                                    <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6.023 16L6 9H3V6h3V4c0-2.7 1.672-4 4.08-4 1.153 0 2.144.086 2.433.124v2.821h-1.67c-1.31 0-1.563.623-1.563 1.536V6H13l-1 3H9.28v7H6.023z" fill="#FFFFFF"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span className="screen-reader-text">Twitter</span>
                                    <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16 3c-.6.3-1.2.4-1.9.5.7-.4 1.2-1 1.4-1.8-.6.4-1.3.6-2.1.8-.6-.6-1.5-1-2.4-1-1.7 0-3.2 1.5-3.2 3.3 0 .3 0 .5.1.7-2.7-.1-5.2-1.4-6.8-3.4-.3.5-.4 1-.4 1.7 0 1.1.6 2.1 1.5 2.7-.5 0-1-.2-1.5-.4C.7 7.7 1.8 9 3.3 9.3c-.3.1-.6.1-.9.1-.2 0-.4 0-.6-.1.4 1.3 1.6 2.3 3.1 2.3-1.1.9-2.5 1.4-4.1 1.4H0c1.5.9 3.2 1.5 5 1.5 6 0 9.3-5 9.3-9.3v-.4C15 4.3 15.6 3.7 16 3z" fill="#FFFFFF"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span className="screen-reader-text">Google</span>
                                    <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.9 7v2.4H12c-.2 1-1.2 3-4 3-2.4 0-4.3-2-4.3-4.4 0-2.4 2-4.4 4.3-4.4 1.4 0 2.3.6 2.8 1.1l1.9-1.8C11.5 1.7 9.9 1 8 1 4.1 1 1 4.1 1 8s3.1 7 7 7c4 0 6.7-2.8 6.7-6.8 0-.5 0-.8-.1-1.2H7.9z" fill="#FFFFFF"/>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                        <div className="footer-copyright">&copy; 2018 Agnes, all rights reserved</div>
                    </div>
                </div>
            </Footer>
        </LayoutContainer>
    </>
)
}

function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
        },
        dispatch
    );
}

let LayoutProjectRedux = connect(
    mapStateToProps,
    mapDispatchToProps
)(LayoutProject);

export default (LayoutProjectRedux)