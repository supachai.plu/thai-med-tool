/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

// You can delete this file if you're not using it
import React from 'react';
import { Provider } from 'react-redux';
import createStore from './src/state/store'
import LayoutProject from './src/pages/layout'

//var window = {}

export const wrapRootElement = ({ element }) => {
    //setupAuth({});

    return (
        <Provider store={createStore()}>
            <LayoutProject>
            {element}
            </LayoutProject>
        </Provider>
    )
}