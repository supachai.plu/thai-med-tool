var app = require('express')();
var cors    = require('cors');
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var moment = require('moment');

const { v4: uuidv4 } = require('uuid');

app.use(cors())

var clients = {}
var admins = {}

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

function sendClientData(){
  Object.keys(admins).forEach(function(key) {
    io.in(admins[key].socket_id).emit("client_data", clients );
  })
}

io.on('connection', (socket) => {
  console.log('a user connected', socket.id);

  socket.on('_join', (data) => {
    console.log( '_join', data )
    if( !clients[data.id] ){
      clients[data.id] = { socket_id: socket.id, active_date: moment(), msg: [] }
    }
    sendClientData()
  })
  /*
  socket.on('_leave', () => { 
    try{
      Object.keys(clients).forEach(function(key) {
        clients[key].forEach( (client, idx) => {
          if( client.id === socket.id ){
            clients[key].slice( idx, 1 )
          }
        })
      })

      socket.emit("leaved", {} );

    }catch(e){
      logger.info( e )
    }
  });
  */

  socket.on('disconnect', () => {
    console.log('user disconnected', socket.id);
    Object.keys(clients).forEach(function(key) {
      //clients[key].forEach( (client, idx) => {
        if( clients[key].socket_id === socket.id ){
          delete clients[key]//.slice( idx, 1 )
        }
      //})
    })
    sendClientData()
  });

  socket.on('chat message', (data) => {
    //console.log('message: ' + data.msg);
    clients[data.id].msg.push(data.msg);
    clients[data.id].active_date = moment();
    //io.emit('admin message', 'Wel Wel Wel');
    //io.in(data.clients).emit("notify me",{"branch_no" : branchNo, station});
    sendClientData()
  });

  socket.on('_join_admin', (data) => {
    //console.log( '_join_admin', data )
    if( !admins[data.id] ){
      admins[data.id] = { socket_id: socket.id, active_date: moment()}
    }
    
    sendClientData()

  })

  socket.on('admin message', (data) => {
    console.log('admin message: ' + data.msg, data.clientId);
    /*
    clients[data.id].msg.push(data.msg);
    clients[data.id].active_date = moment();
    */
    clients[data.clientId].msg.push(data.msg)
    //io.emit('admin message', 'Wel Wel Wel');
    io.in(clients[data.clientId].socket_id).emit("response message", data.msg );
  });

});

http.listen(3000, () => {
  console.log('listening on *:3000');
});